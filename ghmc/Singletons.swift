//
//  Singletons.swift
//  ghmc
//
//  Created by Uday on 20/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit

class Singletons:NSObject {
    
    static let shared = Singletons()
    
    
    func textfieldImage(_ image:UIImage)->UIView{
        let envelopeView = UIImageView(frame: CGRect(x: 5, y: 10, width: 25, height: 25))
        envelopeView.image = image
        envelopeView.contentMode = .center
        let viewLeft: UIView = UIView(frame: CGRect(x: 20, y: 0, width: 45, height: 40))// set per your requirement
        viewLeft.addSubview(envelopeView)
        return viewLeft
    }

}
