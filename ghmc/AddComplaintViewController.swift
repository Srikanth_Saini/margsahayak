//
//  AddComplaintViewController.swift
//  ghmc
//
//  Created by Uday on 22/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit
import DropDown
import MapKit

class AddComplaintViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var DescriptionTf: UITextField!
    @IBOutlet weak var SelectGrievenceLabel: UILabel!
    @IBOutlet weak var whiteCardView: UIView!
    @IBOutlet weak var ComplaintImageView: UIImageView!
    var isLocationUpadated = false
    let dropDown = DropDown()
    var Imagedata:Data?
    var date = NSDate()
    var location:CLLocation? = nil
    var locationManager: CLLocationManager!
    
    var Grievances = [grievancename]()
    var GNames = [String]()
    
    //TOPostParameterforAddComplaint
    
    var RoadNo:String?
    var ImageName:String?
    var Roadname:String?
    var griev_type:String?
    var DDescription:String?
    var OTP = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        getCommonOTpMethod(Complition: {
//                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
//                    self.GetVerifyOTPMethod()
//                })
//        })
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        //Adding Tap Gesture to SelectGrievenceLabel to identify userTap
        let tap = UITapGestureRecognizer(target: self, action: #selector(selectGrivenveAction))
        SelectGrievenceLabel.addGestureRecognizer(tap)
        SelectGrievenceLabel.isUserInteractionEnabled = true
        SelectGrievenceLabel.set(text: "Select Grievance type", leftIcon: UIImage(), rightIcon: UIImage.init(named: "dropdown"))
        dropDown.anchorView = SelectGrievenceLabel
        dropDown.center = self.view.center
        dropDown.dataSource = ["DAMAGED BRIDGE( પુલ ને નુકશાન )", "DAMAGED BRIDGE PARAPET( પુલ ની પેરાપેટને નુકશાન )", "BREACH ON A ROAD ( રસ્તા પર ભંગ )", "DAMAGED RAILING( રેલિંગ ને નુકશાન )", "DAMAGE STRUCTURES( મકાન ને નુકશાન )", "POT HOLES( પોટહોલ )", "FALLEN TREE( ઝાડ પડેલ છે )", "DEGREDED ROADS(નષ્ટ રસ્તાઓ)", "Road/Bridge Overtopped( રોડ/પુલ પર પાણી ભરાઈ જાય છે )"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.SelectGrievenceLabel.text = item
        }
        DropDown.startListeningToKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SelectGrievenceLabel.backgroundColor = .white
        self.whiteCardView.CreateShadow(cornerRadius: 5.0, shadowColor: .gray)
        self.DescriptionTf.addBottomBorder()
        self.SelectGrievenceLabel.CreateShadow(cornerRadius: 5.0, shadowColor: .gray)
        DescriptionTf.tintColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        DescriptionTf.textColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        
    }
    
    @IBAction func DismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- CLLOcation Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        self.location = location
        
        if isLocationUpadated == false{
            if self.location != nil{
                isLocationUpadated = true
            getCommonOTpMethod(Complition: {
            
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self.GetVerifyOTPMethod()
                })
        })
            }
        }
        
    }
    
    
    //MARK:- AlertView
    
    func CreatAlertViewToAddImage(){
        
        self.popupAlert(title: "Add Photo", message: "Choose an option", Style: .actionSheet, actionTitles: ["Camera","Gallery","Cancel"], actions: [{
            cameraAction in
            let imagepicker = UIImagePickerController()
            imagepicker.delegate = self
            imagepicker.sourceType = .camera
            imagepicker.allowsEditing = true
            self.present(imagepicker, animated: true, completion: nil)
            },{
                GalleryAction in
                let imagepicker = UIImagePickerController()
                imagepicker.delegate = self
                imagepicker.sourceType = .photoLibrary
                imagepicker.allowsEditing = true
                self.present(imagepicker, animated: true, completion: nil)
                
            },nil])
        
        
    }
    
    //MARK:- ImagePicker Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else{return}
        self.ComplaintImageView.image = image
        let info = info.description as NSString
        print(info.lastPathComponent)
        //convertImageToData
        DispatchQueue.main.async {
            guard let imageData = image.jpegData(compressionQuality: 0.4) else {
                return
            }
            self.Imagedata = imageData
            let dataformatter = DateFormatter()
            dataformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let Datestring = dataformatter.string(from: Date())
            self.UPloadImage(imageName: "\(Datestring).jpeg")
        }
        
        
        
    }
    
    //MARK:- actions
    
    @IBAction func UploadAction(_ sender: Any) {
        
        guard NetworkState.isConnected() else{
            
            self.popupAlert(title: "", message: "You currently don't have an \n active internet connection \n Want to upload complaint later?", Style: .alert, actionTitles: ["CANCEL","SAVE OFFLINE"], actions: [{
                action1 in
                },{
                    action2 in
                    guard self.Imagedata != nil else{
                        self.popupAlert(title: "Please add complaint image", message: "", Style: .alert, actionTitles: ["OK"], actions: [nil])
                        return}
                    
                    guard self.location != nil else{
                    self.popupAlert(title: "Co ordinates not available", message: "Goto ->settings->marg sahayak->Location->choose Allow location access option except 'Never'", Style: .alert, actionTitles: ["OK"], actions: [nil])
                         return
                        
                    }
                    
                    guard (self.SelectGrievenceLabel.text?.contains("Select Grievance"))! == false else{
                        self.popupAlert(title: "Select grievance type", message: "", Style: .alert, actionTitles: ["OK"], actions: [nil])
                        return}
                   
                    let isSaved = CoredataHandler.shared.saveComplaint(self.Imagedata!, self.location!.coordinate.latitude, self.location!.coordinate.longitude, "testing", self.SelectGrievenceLabel.text!, Date())
                    if isSaved{
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.popupAlert(title: "Something went wrong", message: "please try again", Style: .alert, actionTitles: ["OK"], actions: [nil])
                    }
                }])
            
            return
        }
        
        self.AddComplaint()
    }
    
    
    
    @IBAction func AddImageButton(_ sender: Any) {
        //This button is added on top of the complaintImageView in storyboard.
        CreatAlertViewToAddImage()
    }
    
    @objc func selectGrivenveAction(){
//        getGrievance()
        dropDown.show()
    }
    
}


//MARK:- UploadImage

extension AddComplaintViewController{
    
    
    
    
    func UPloadImage(imageName:String){
        
        guard NetworkState.isConnected() else{return}
        var r  = URLRequest(url: URL(string: AddImage_Url)!)
        r.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        r.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        r.httpBody = createBody(boundary: boundary, data: self.Imagedata!, mimeType: "image/jpg", filename: imageName)
        let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
        r.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
        
        let task = URLSession.shared.dataTask(with: r, completionHandler: {
            (data,response,error) in
            guard error == nil, let data = data else{
                DispatchQueue.main.async {
                    
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                }
                return
            }
            
            do{
                
                let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                let success = dataResponse["success"] as? Bool
                print(dataResponse)
                DispatchQueue.main.async {
                    if success == true{
                        self.ImageName = dataResponse["data"] as? String
                        
                    }else{
                        
                        self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                        
                    }
                }
                
                
            }catch{
                DispatchQueue.main.async {
                    
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                }
            }
            
            
            
        })
        task.resume()
    }
    
    func createBody(boundary: String,
                    data: Data,
                    mimeType: String,
                    filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        //        for (key, value) in parameters {
        //            body.appendString(boundaryPrefix)
        //            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
        //            body.appendString("\(value)\r\n")
        //        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }
    
    
    // GET GRIEvance
       
       func getGrievance(){
           guard NetworkState.isConnected() else {
                  self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                  return
              }
           var request = URLRequest(url: URL.init(string: grienvances_Url)!)
           request.httpMethod = "GET"
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
           request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
           let task = URLSession.shared.dataTask(with: request, completionHandler: {
               (data,response,error) in
               guard error == nil, let data = data else{
                    DispatchQueue.main.async {
                       self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                   }
                   return
               }
               
               do{
                   let Grievance = try JSONDecoder().decode(grienvanceModel.self, from: data)
                   self.Grievances = Grievance.data
                   var names = [String]()
                   for g in self.Grievances{
                       names.append(g.name!)
                       
                   }
                   self.GNames = names
                   DispatchQueue.main.async {
                       self.dropDown.dataSource = self.GNames
                       self.dropDown.reloadAllComponents()
                       self.dropDown.show()
                   }
                   
               }catch{
                   DispatchQueue.main.async {
                       
                       self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                   }
               }
           })
           task.resume()
           
           
       }
    
    
    func getCommonOTpMethod(Complition: @escaping ()->(Void)){
        
        guard NetworkState.isConnected() else {
            self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        let MobileNumber = UserDefaults.standard.value(forKey: UserdefaultKeys.MobileNumber.rawValue) as! String
        
        
                  let data : Data = "MobileNumber=\(MobileNumber)&appName=Citizen Sentinel".data(using:String.Encoding.ascii, allowLossyConversion: false)!
                  var request = URLRequest(url: URL.init(string: "https://edistrict.ncog.gov.in/RNB_mob_data/CommanOtpMethod")!)
                  request.httpMethod = "POST"
                  request.httpBody = data
                  request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                  
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    
                        guard error == nil, let data = data else{
                            DispatchQueue.main.async {
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            }
                           return
                        }
                        
                        do{
                            
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            self.OTP = (dataResponse["otp"] as? String)!
                            print(self.OTP)
                           Complition()
                            
                        }catch{
                            DispatchQueue.main.async {
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            }
                            
                        }
                    
                  })
                  task.resume()
                  
       
        
       
        
    }
    
    func GetVerifyOTPMethod(){
        
        
        guard self.location != nil else{
            self.popupAlert(title: "Co ordinates not available", message: "Goto ->settings->marg sahayak->Location->choose Allow location access option except 'Never'", Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        let lat = "\(self.location!.coordinate.latitude)"
        let long = "\(self.location!.coordinate.longitude)"
        let MobileNumber = UserDefaults.standard.value(forKey: UserdefaultKeys.MobileNumber.rawValue) as! String
         
                  let data : Data = "MobileNumber=\(MobileNumber)&appName=Citizen Sentinel&otpMessage=\(self.OTP)&latitude=\(lat)&longitude=\(long)".data(using:String.Encoding.ascii, allowLossyConversion: false)!
                   
                  var request = URLRequest(url: URL.init(string: "https://edistrict.ncog.gov.in/RNB_mob_data/VerifayOTPMethod")!)
                  request.httpMethod = "POST"
                  request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                  request.httpBody = data
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    DispatchQueue.main.async {
                        guard error == nil, let data = data else{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                           return
                        }
                        
                        do{
                            self.Indicator.stopAnimating()
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            print(dataResponse)
                            
                            if dataResponse["status"] as? String == "true"{
                                let AvailabelDic = dataResponse["data"] as! [[String:Any]]
                                self.RoadNo = AvailabelDic[0]["uniqe_code"] as? String
                                self.Roadname = AvailabelDic[0]["name"] as? String
                                
                            }
                            
                            if dataResponse["status"] as? String == "false"{
                                self.errorHandlingApi()
                            }
                            
                        }catch{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            
                        }
                    }
                  })
                  task.resume()
                  
       
        
    }
    
    
    func errorHandlingApi(){

        let MobileNumber = UserDefaults.standard.value(forKey: UserdefaultKeys.MobileNumber.rawValue) as! String
        let parameters:[String:Any] = ["phoneNo":MobileNumber,"location": [ 23.234223, 72.245433 ],"message":" Please Enter Valid OTP."]

          do {
                  let data = try JSONSerialization.data(withJSONObject: parameters, options: [])

                  var request = URLRequest(url: URL.init(string: "http://roadgrievancealpha.herokuapp.com/api/android/error")!)
                  request.httpMethod = "POST"
                  request.httpBody = data
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    DispatchQueue.main.async {
                        guard error == nil, let data = data else{
                            self.Indicator.stopAnimating()

                           return
                        }

                        do{
                            self.Indicator.stopAnimating()
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            print(dataResponse)

                        }catch{
                            self.Indicator.stopAnimating()


                        }
                    }
                  })
                  task.resume()

        }catch{
            self.Indicator.stopAnimating()

        }

    }
    
    
    
    func AddComplaint(){
        guard self.ImageName != nil else{
            self.popupAlert(title: "", message: "Please add complaint image", Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        guard self.location != nil else{
                   self.popupAlert(title: "Co ordinates not available", message: "Goto ->settings->marg sahayak->Location->choose Allow location access option except 'Never'", Style: .alert, actionTitles: ["OK"], actions: [nil])
                   return
               }
        
        guard self.Roadname != nil,self.RoadNo != nil, (self.SelectGrievenceLabel.text?.contains("Select Grievance"))! == false  else{
            self.popupAlert(title: "", message: "Select grievance Type", Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
       
        
        Indicator.startAnimating()
        
        let parameters:[String:Any] = ["url":self.ImageName!,"road_code":self.RoadNo!,"name":self.Roadname!, "griev_type":self.SelectGrievenceLabel.text!, "location": [ self.location?.coordinate.latitude , self.location?.coordinate.longitude ], "description":"Testing of Application. Not real Complaint"]
               print(parameters)
                 do {
                         let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
                                
                         var request = URLRequest(url: URL.init(string: "http://roadgrievancealpha.herokuapp.com/api/android/postNewComplaint")!)
                         request.httpMethod = "POST"
                         request.httpBody = data
                         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                         let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
                    request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
                         let task = URLSession.shared.dataTask(with: request, completionHandler: {
                             data,response,error in
                           DispatchQueue.main.async {
                               guard error == nil, let data = data else{
                                   self.Indicator.stopAnimating()
                                   self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                  return
                               }
                               
                               do{
                                   self.Indicator.stopAnimating()
                                   let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                                   let success = dataResponse["success"] as? Bool
                                if success ?? false{
                                    self.Roadname = String()
                                    self.RoadNo = String()
                                    self.popupAlert(title: "", message: "Complaint added successfully", Style: .alert, actionTitles: ["OK"], actions: [{
                                        action in
                                        self.dismiss(animated: true, completion: nil)
                                        }])
                                }
                                   
                               }catch{
                                   self.Indicator.stopAnimating()
                                   self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                   
                               }
                           }
                         })
                         task.resume()
                         
               }catch{
                   self.Indicator.stopAnimating()
                     
               }
        
    }
    
}


