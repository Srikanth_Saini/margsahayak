//
//  DetailTableViewCell1.swift
//  ghmc
//
//  Created by Sraoss on 28/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit

class DetailTableViewCell1: UITableViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var StatusLabel:UILabel!
    @IBOutlet weak var GrievanceLabel:UILabel!
    @IBOutlet weak var ComplaintIdLabel:UILabel!
    @IBOutlet weak var SubmitTimeLabel:UILabel!
    @IBOutlet weak var SubmitDateLabel:UILabel!
    @IBOutlet weak var RoadNameLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backView.CreateShadow(cornerRadius: 0.0, shadowColor: .lightGray)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
