//
//  CoredataHandler.swift
//  ghmc
//
//  Created by Uday on 22/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoredataHandler{
    
    static let shared = CoredataHandler()
    
    //MARK:- create
    
    func saveComplaint(_ image:Data, _ latitude:Double, _ longitude:Double, _ comment:String, _ Grievance:String, _ date:Date) -> Bool{
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return false}

        let managedContext = appDelegate.persistentContainer.viewContext
        //let entity = NSEntityDescription.entity(forEntityName: "Complaint", in: managedContext)
        let entity = NSEntityDescription.insertNewObject(forEntityName: "Complaint", into: managedContext)
        entity.setValue(image, forKey: "imagedata")
        entity.setValue(latitude, forKey: "latitude")
        entity.setValue(longitude, forKey: "longitude")
        entity.setValue(comment, forKey: "comment")
        entity.setValue(Grievance, forKey: "grievance")
        entity.setValue(date, forKey: "date")
        do{
            try managedContext.save()
            return true
        }catch{
            
            return false
        }
        
    }
    
    //MARK:- Read
    
    func GetSavedDataFromCoredata() -> [NSManagedObject]{
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return []}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Complaint")
        do{
            let result = try managedContext.fetch(fetchRequest)
            return result as! [NSManagedObject]
        }catch{
            return []
        }
        
        
    }
    
    
    //MARK:- Delete
    
    func DeleteRecord(_ date:NSDate){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Complaint")
        fetchRequest.predicate = NSPredicate(format: "date =%@", date)
        do{
            let test = try managedContext.fetch(fetchRequest)
            guard test.count > 0 else{return}
            let objectToDelete = test[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            do{
                
                try managedContext.save()
                
            }catch{
                print("not saved after deleting")
            }
            
        }catch{
            print("Not Deleted")
        }
        
        
    }
    
    
    func resetAllRecords(_ entity : String)
    {

        let context = ( UIApplication.shared.delegate as! AppDelegate ).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false

        do
        {
            let results = try context.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                context.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all my data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    
    
    
}
