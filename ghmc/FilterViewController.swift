//
//  FilterViewController.swift
//  ghmc
//
//  Created by Sraoss on 26/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit

protocol FilterTableReloadDelegate{
    func ReloadFilterTableView()
}

var FilterComplaints = [recoverComplaints]()
class FilterViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,FilterTableReloadDelegate {
    
    

    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var FilterTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FilterTableView.delegate = self
        FilterTableView.dataSource = self
        GetFilterComplaintsForFilter()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       // FilterTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(FilterComplaints)
        //FilterTableView.reloadData()
    }
    
    func ReloadFilterTableView() {
        FilterTableView.reloadData()
    }
    
    @IBAction func FilterAction(_ sender: Any) {
        self.performSegue(withIdentifier: "FilterMenu", sender: self)
    }
    
    @IBAction func DismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if FilterComplaints.count == 0 {
            self.FilterTableView.setEmptyMessage("No complaints found")
        } else {
            self.FilterTableView.restore()
        }
        return FilterComplaints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FCell", for: indexPath) as! ComplaintsTableViewCell
        let item = FilterComplaints[indexPath.row]
        cell.grevianceLabel.text = item.griev_type
        cell.TimeLabel.text = "Reported on:\(item.complaint_upload_date ?? "")"
        cell.StausLabel.text = "Status:\(item.complaint_status ?? "")"
        let imgUrl = (item.url)!.replace(target: " ", withString:"%20")
        cell.ComplaintImageView.kf.setImage(with: URL(string: imgUrl))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "HomeDetail") as! DetailComplaintViewController
            vc.complaint = FilterComplaints[indexPath.row]
            vc.modalPresentationStyle = .fullScreen
            
            self.present(vc, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeDetail") as! DetailComplaintViewController
            vc.complaint = FilterComplaints[indexPath.row]
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:- segue Action
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterMenu"{
            let vc = segue.destination as! FilterMenuViewController
            vc.delegate = self
        }
    }
    
}


//MARK:- PARSING ONLINE COMPLAINTS

extension FilterViewController{

func GetFilterComplaintsForFilter(){
    guard NetworkState.isConnected() else {
        self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
        return
    }
    Indicator.startAnimating()
    var request = URLRequest(url: URL.init(string: RecoverComplaints_Url)!)
    request.httpMethod = "GET"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
    request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
    let task = URLSession.shared.dataTask(with: request, completionHandler: {
        (data,response,error) in
        guard error == nil, let data = data else{
             DispatchQueue.main.async {
                self.Indicator.stopAnimating()
                self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            }
            return
        }
        
        do{
            let complaints = try JSONDecoder().decode([recoverComplaints].self, from: data)
            FilterComplaints = complaints
            
            DispatchQueue.main.async {
                self.Indicator.stopAnimating()
                self.FilterTableView.reloadData()
                 self.performSegue(withIdentifier: "FilterMenu", sender: self)
            }
           

        }catch{
            DispatchQueue.main.async {
                self.Indicator.stopAnimating()
                self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            }
        }
        
        
        
    })
    task.resume()
    
}

}

